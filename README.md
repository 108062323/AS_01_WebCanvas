# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 
![image](text.png)  文字輸入，按到畫布上可以進行輸入，按enter才會寫入完成。大小用input text的數字，顏色則是input color，字型也可以在上面選擇。  
![image](clear.png)   清空畫布  
![image](pen.png)畫筆，粗細由上面的slider bar操作，顏色則是上面的input color。  
![image](eraser.png)橡皮擦，粗細一樣由上面slider決定。  
![image](oval.png)圓圈，可以畫圓，按下最右邊的checkbox可以畫填充的圖形，顏色一樣是上面的color。  
![image](triangle.png)三角形，按下最右邊的checkbox可以畫填充的圖形，顏色一樣是上面的color。  
![image](rectangle.png)長方形，按下最右邊的checkbox可以畫填充的圖形，顏色一樣是上面的color。  
![image](undo.png)undo，按下後回到上一步。  
![image](redo.png)redo，按下後回到下一步。   
![image](upload.png)按下後可以上傳圖片。  
![image](download.png)按下後可以下載圖片。  

### Function description
![image](rainbow.png)彩虹筆，畫下去畫筆有彩虹的效果。  
![image](grayscale.png)灰階，按下去後會呈現灰階的效果。  
![image](constrast.png)負片效果，按下去後會呈現原本的補色。  
![image](mirrorup.png)上下翻轉，按下去後圖片顛倒。  
![image](mirrorleft.png)左右翻轉，按下去後圖片左右顛倒。  
![image](line.png)畫直線的功能。  
### Gitlab page link

    https://108062323.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
