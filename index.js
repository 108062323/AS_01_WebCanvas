var can;
var ctx;
var mode;
var erase;
var ing = false;
var radius;
var rad = 1;
var num=-1;
var record = [];
var posx;
var posy;
var drawingSurfaceImageData;
var cradius;
var colorselector;
var color = "black";
var istype;
var font;
var fontsize = '16';
var fill = false;
var rainbow;
window.onload = function(){
    can = document.getElementById("myCanvas");
    can.width = "500";
    can.height = "380";
    can.style.border="5px solid black"; 
    
    colorselector = document.getElementById("brushcolor");
    colorselector.addEventListener("input", changecolor, false);
    colorselector.select();
    ctx = can.getContext('2d');
    can.addEventListener('mousedown',selectmode);
    can.addEventListener('mousemove',Doing);
    can.addEventListener('mouseup',stop);
    can.addEventListener('click',starttype);
    radius = document.getElementById("radius");
    radius.onchange = function(){
        rad = this.value;
    };
    var img = document.getElementById("inputimage");
    img.addEventListener("change",upload,false);
      
}
function startdraw(e){
    ctx.lineWidth=rad;
    ctx.beginPath();
    ctx.moveTo(posx,posy);
}

function changecolor(e){
     color =  e.target.value;
     ctx.fillStyle = color;
}

function drawshape(e){
    var currentx,currenty;
    currentx = e.pageX-can.offsetLeft;
    currenty = e.pageY-can.offsetTop;
    fill = document.getElementById("fill").checked;
    if(ing!= true)return;
    ctx.putImageData(drawingSurfaceImageData, 0, 0);
    w = currentx - posx;
    h = currenty - posy;
    if(mode == 'circle'){
        cradius = Math.sqrt(Math.pow(Math.abs(currentx - posx), 2) + Math.pow(Math.abs(currenty - posy), 2));
        ctx.beginPath();
        ctx.arc(posx, posy, cradius, 0, Math.PI * 2, false);
        if(fill)ctx.fill();
        else ctx.stroke();   
    }
    else if(mode == 'rectangle'){
        if(fill)ctx.fillRect(posx, posy, w, h);
        else ctx.strokeRect(posx, posy, w, h);   
    }
    else if(mode == 'line'){
        ctx.lineWidth=rad;
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        ctx.beginPath();
        ctx.moveTo(posx, posy);
        ctx.lineTo(posx+w, posy+h);
        ctx.stroke();
    }
    else if(mode == 'triangle'){
        ctx.beginPath();
        ctx.moveTo(posx+w/2, posy);
        ctx.lineTo(posx+w, posy+h);
        ctx.lineTo(posx, posy+h);
        ctx.lineTo(posx+w/2, posy);
        if(fill)ctx.fill();
        else ctx.stroke(); 
    }
}

function drawing(e){
    if ((mode == 'draw' ||mode=='drawrainbow') && ing == true){
        if(rainbow==true){
            ctx.strokeStyle = `hsl(${ hue }, 50%, 80%)`;
            if (hue >= 360) hue = 0;
            hue++;
        }
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        ctx.beginPath();
        ctx.moveTo(posx,posy);
        ctx.lineTo(e.pageX-can.offsetLeft,e.pageY-can.offsetTop);
        ctx.stroke();
        posx = e.pageX-can.offsetLeft;
        posy = e.pageY-can.offsetTop;
        ctx.closePath();
    }
}

function erasing(e){
    if (mode == 'erase' && ing == true){
        ctx.save();
        ctx.beginPath();
        ctx.arc(e.pageX-can.offsetLeft,e.pageY-can.offsetTop,rad,0,Math.PI*2);
        ctx.clip();
        ctx.clearRect(0,0,can.width,can.height);
        ctx.restore();
    }
}
function stop(){
    ing = false;
    if(mode == 'draw' || mode=='erase' || mode=='rectangle' || mode=='circle' || mode=='triangle' ||mode=='drawrainbow'|| mode=='line' ){
        if(record.length>num+1)record.length=num+1;
        record.push(ctx.getImageData(0, 0, can.width, can.height));
        num +=1;
    }
}

function clearcanvas(){
    document.getElementById("myCanvas").style.cursor = "default";
    mode = 'clear';
    setmode();
    ctx.clearRect(0,0,can.width,can.height);
}

function erasemode(){
    mode = 'erase';
    document.getElementById("myCanvas").style.cursor = 'url(era.png),pointer';
    setmode();
}

function drawmode(){
    document.getElementById("myCanvas").style.cursor = 'url(penc.png),pointer';
    mode = 'draw';
    setmode();
}

function drawrainbow(){
    document.getElementById("myCanvas").style.cursor = 'url(rainbowc.png),pointer';
    mode = 'drawrainbow';
    setmode();
}

function rectanglemode(){
    document.getElementById("myCanvas").style.cursor = 'url(rectanglec.png),pointer';
    mode = 'rectangle';
    setmode();
}

function trianglemode(){
    document.getElementById("myCanvas").style.cursor = 'url(trianglec.png),pointer';
    mode = 'triangle';
    setmode();
}

function linemode(){
    mode = 'line';
    document.getElementById("myCanvas").style.cursor = "default";
    setmode();
}

function circlemode(){
    document.getElementById("myCanvas").style.cursor = 'url(circlec.png),pointer';
    mode = 'circle';
    setmode();
}

let hue;

function selectmode(e){
    posx = e.pageX-can.offsetLeft;
    posy = e.pageY-can.offsetTop;
    ctx.strokeStyle= color;
    hue=0;
    if(mode=="drawrainbow"){
      rainbow=true;
    }
    else{
        rainbow=false;
        ctx.strokeStyle= color;
    }
    ing = true;
    if(mode == 'draw' || mode =='drawrainbow'){
        startdraw(e);
    }
    else if(mode == 'circle' ||mode == 'triangle'|| mode == 'rectangle' ||mode =='line'){
        drawingSurfaceImageData = ctx.getImageData(0, 0, can.width, can.height);
    }
    else return;
}


function Doing(e){
    if(mode == 'draw' || mode=='drawrainbow'){
        drawing(e);
    }
    else if(mode == 'erase'){
        erasing(e);
    }
    else if(mode == 'rectangle' || mode == 'circle' || mode == 'triangle'|| mode=='line'){
        drawshape(e);
    }
}

function savefile(){
    mode='save';
    setmode();
    document.getElementById("myCanvas").style.cursor = "default";
    var sf = document.createElement('a');
    sf.href = can.toDataURL('image/png');
    sf.download = 'pic' +(new Date).getTime();
    sf.click();
}

function undo(){  
    mode='undo';
    setmode();
    document.getElementById("myCanvas").style.cursor = "default";
    if(num>0){
        ctx.putImageData(record[num-1], 0, 0);
        num-=1;
    }
    else{
        num = -1;
        ctx.clearRect(0,0,can.width,can.height);
    }
}
function redo(){  
    mode='redo';
    setmode();
    document.getElementById("myCanvas").style.cursor = "default";
    if(num<record.length-1){
        ctx.putImageData(record[num+1], 0, 0);
        num+=1;
    }
    else{
        num = record.length-1;
    }
}

function inputtxt(){
    mode = 'txt';
    setmode();
    document.getElementById("myCanvas").style.cursor = "text";
}

function starttype(e){
    if(mode == 'txt'  && istype !=true){
        var size=document.getElementById("textsize").value;
        ctx.font = size+'px'+' '+font;
        posx = e.pageX-can.offsetLeft;
        posy = e.pageY-can.offsetTop;
        var inputtxt= document.createElement('input');
        inputtxt.type = 'text';
        inputtxt.style.position = 'fixed';
        inputtxt.style.left = 171.9+posx +'px';
        inputtxt.style.top = posy+92.9+94.3+8 +'px';
        inputtxt.onkeydown = typing;
        document.body.appendChild(inputtxt);
        inputtxt.focus();
        istype = true;
    }
}

function typing(e){
    if(mode == 'txt'){
        if(e.key == 'Enter'){
            ctx.fillText(this.value,posx,posy);
            document.body.removeChild(this);
            console.log(e.key);
            istype = false;
            if(record.length>num+1)record.length=num+1;
            record.push(ctx.getImageData(0, 0, can.width, can.height));
            num +=1;
        }
    }
}

function upload(e){
    mode='inputimage';
    setmode();
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload = function(e){
        var newimage= new Image();
        newimage.src = e.target.result;
        newimage.onload = function(){
            let p= new Promise(function(resolve,reject){
                ctx.drawImage(newimage,0,0);
                resolve(1);
            })
            p.then(function(value){
                if(record.length>num+1)record.length=num+1;
                record.push(ctx.getImageData(0, 0, can.width, can.height));
                num +=1;
            })
        }
    }
}

function changefont(){
    font =document.getElementById("selectfont").value;
}

function mirrorup(){
    mode='mirrorup';
    setmode();
    pic = ctx.getImageData(0, 0, can.width, can.height);
    ctxdata = pic.data;
    let newone = [];
    let a;
    h = can.height;
    w = can.width;
    var aper;
    var bper;
      for (var i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
          aper = (i*w+j)*4;
          bper = (h-i) * w * 4 + j * 4;
          newone[aper] = ctxdata[bper];
          newone[aper+1] = ctxdata[bper+ 1];
          newone[aper+2] = ctxdata[bper + 2];
          newone[aper+3] = ctxdata[bper + 3];
        }
    }

    for(let j = 0; j < ctxdata.length; j++) {
		pic.data[j] = newone[j];
	}

    ctx.putImageData(pic, 0, 0);
    if(record.length>num+1)record.length=num+1;
    record.push(ctx.getImageData(0, 0, can.width, can.height));
    num +=1;
}
function mirrorleft(){
    mode='mirrorleft';
    setmode();
    pic = ctx.getImageData(0, 0, can.width, can.height);
    ctxdata = pic.data;
    let newone = [];
    let a;
    h = can.height;
    w = can.width;
    var per;
    var iper;
    for (var i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
          per = (i * w +j) * 4;
          iper = i * w * 4 + (w - j) * 4;
          newone[per ] = ctxdata[iper];
          newone[per + 1] = ctxdata[iper + 1];
          newone[per + 2] = ctxdata[iper + 2];
          newone[per + 3] = ctxdata[iper + 3];
        }
    }
    for(let j = 0; j < ctxdata.length; j++) {
		pic.data[j] = newone[j];
	}
    ctx.putImageData(pic, 0, 0);
    if(record.length>num+1)record.length=num+1;
    record.push(ctx.getImageData(0, 0, can.width, can.height));
    num +=1;
}

function contrast(){
    mode='contrast';
    setmode();
    pic = ctx.getImageData(0, 0, can.width, can.height);
    ctxdata = pic.data;
    let q,w,e,r;
    let avg;
    for(let i=0;i<ctxdata.length;i+=4){
        ctxdata[i]=255-ctxdata[i];
        ctxdata[i+1]=255-ctxdata[i+1];
        ctxdata[i+2]=255-ctxdata[i+2];
    }
    ctx.putImageData(pic, 0, 0);
    if(record.length>num+1)record.length=num+1;
    record.push(ctx.getImageData(0, 0, can.width, can.height));
    num +=1;
}
var lastmode;
function setmode(){
    if(lastmode!=null){
        var last=document.getElementById(lastmode);
        last.style.backgroundColor="#EC5768";
        last.style.opacity="1";
    }
    var now=document.getElementById(mode);
    if(now!=null){
        now.style.backgroundColor="white";
        now.style.opacity="1";
    }
    lastmode=mode;
}
function grayscale(){
    mode='grayscale';
    setmode();
    pic = ctx.getImageData(0, 0, can.width, can.height);
    ctxdata = pic.data;
    let q,w,e,r;
    let avg;
    for(let i=0;i<ctxdata.length;i+=4){
        q=ctxdata[i];
        w=ctxdata[i+1];
        e=ctxdata[i+2];
        r=ctxdata[i+3];
        avg=(q+w+e)/3;
        ctxdata[i]=avg;
        ctxdata[i+1]=avg;
        ctxdata[i+2]=avg;
        ctxdata[i+3]=r;
    }
    ctx.putImageData(pic, 0, 0);
    if(record.length>num+1)record.length=num+1;
    record.push(ctx.getImageData(0, 0, can.width, can.height));
    num +=1;
}